﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;

namespace Maze_Escape
{
    class Level
    {
        // ---------------------------
        // Data
        // ---------------------------
        Player player;
        Wall[,] walls;
        List<Coin> coins = new List<Coin>();
        List<Spikes> spikes = new List<Spikes>();
        List<Enemy> enemies = new List<Enemy>();
        int tileWidth, tileHeight;
        int levelWidth, levelHeight;
        Text score;
        int currentLevel = 0;
        bool shouldReload = false;
        Text lives;
        Game1 game;

        Texture2D playerSprite;
        Texture2D wallSprite;
        Texture2D coinSprite;
        Texture2D spikeSprite;
        Texture2D enemySprite;
        SpriteFont UIFont;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public void LoadContent(ContentManager content, GraphicsDevice device, Game1 newGame)
        {
            game = newGame;
            playerSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;
            coinSprite = content.Load<Texture2D>("graphics/Coin");
            spikeSprite = content.Load<Texture2D>("graphics/Spike");
            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            enemySprite = content.Load<Texture2D>("graphics/EnemyAnimation");


            // Set up UI
            score = new Text(UIFont);
            score.SetAlignment(Text.Alignment.TOP_LEFT);
            score.SetPosition(new Vector2(10, 10));

            lives = new Text(UIFont);
            lives.SetAlignment(Text.Alignment.TOP_RIGHT);
            lives.SetPosition(new Vector2(device.Viewport.Bounds.Width - 10,10));

            // Create the player
            player = new Player(playerSprite, 100, 100, 10f, this);
        }
        // -------------------------------------
        public void ResetPlayer(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }
        // -------------------------------------
        public void CreateWall(int tileX, int tileY)
        {
            Wall newWall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newWall.SetPosition(tilePosition);
            walls[tileX, tileY] = newWall;
        }
        // -------------------------------------
        public void CreateCoin(int tileX, int tileY)
        {
            Coin newCoin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newCoin.SetPosition(tilePosition);
            coins.Add(newCoin);
        }
        // -------------------------------------
        public void CreateSpike(int tileX, int tileY)
        {
            Spikes newSpike = new Spikes(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newSpike.SetPosition(tilePosition);
            spikes.Add(newSpike);
        }
        // -------------------------------------
        public void CreateEnemy(int tileX, int tileY, Enemy.MovementType movement)
        {
            Enemy newTile = new Enemy(enemySprite, 100, 100, 10f, movement);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newTile.SetPosition(tilePosition);
            enemies.Add(newTile);
        }
        // -------------------------------------
        public void Update(GameTime gameTime)
        {
            // Update all Actors
            player.Update(gameTime);

            // Collision checking

            // Walls
            List<Wall> collidingTiles = GetWallsInBounds(player.GetBounds());
            foreach (Wall collidingTile in collidingTiles)
            {
                player.HandleCollision(collidingTile);
            }

            // Coins
            foreach (Coin eachCoin in coins)
            {
                // Only collide if the coin is visible
                if (eachCoin.GetVisible() && eachCoin.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachCoin);
                }
            }

            // Update score
            score.SetString("Score: "+player.GetScore());

            // Update lives
            lives.SetString("Lives: " + player.GetLives());

            // Spikes
            foreach (Spikes eachSpike in spikes)
            {
                // Only collide if the coin is visible
                if (!shouldReload && eachSpike.GetBounds().Intersects(player.GetBounds()))
                {
                    eachSpike.HandleCollision(player);
                }
            }

            // Enemies
            foreach (Enemy eachEnemy in enemies)
            {
                eachEnemy.Update(gameTime);
                // Check enemy collisions with walls
                List<Wall> enemyWallTiles = GetWallsInBounds(eachEnemy.GetBounds());
                foreach (Wall collidingTile in enemyWallTiles)
                {
                    eachEnemy.HandleCollision(collidingTile);
                }
                // collide with player
                if (!shouldReload && eachEnemy.GetBounds().Intersects(player.GetBounds()))
                {
                    eachEnemy.HandleCollision(player);
                }
            }


            // If a reload was requested, it is now safe to do it.
            if (shouldReload)
            {
                LoadLevel(currentLevel);
                shouldReload = false;
            }
        }
        // -------------------------------------
        public List<Wall> GetWallsInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> wallsInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetWall(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        wallsInBounds.Add(thisTile);
                }
            }

            return wallsInBounds;
        }
        // -------------------------------------
        public Wall GetWall(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw player
            player.Draw(spriteBatch);

            // Draw walls
            for (int x = 0; x < levelWidth; ++x)
            {
                for (int y = 0; y < levelHeight; ++y)
                {
                    if (walls[x, y] != null)
                        walls[x, y].Draw(spriteBatch);
                }
            }

            // Draw coins
            foreach(Coin eachCoin in coins)
            {
                eachCoin.Draw(spriteBatch);
            }

            // Draw spikes
            foreach (Spikes eachSpike in spikes)
            {
                eachSpike.Draw(spriteBatch);
            }

            // Draw enemies
            foreach (Enemy eachEnemy in enemies)
            {
                eachEnemy.Draw(spriteBatch);
            }

            // Draw UI
            score.Draw(spriteBatch);
            lives.Draw(spriteBatch);
        }
        // -------------------------------------
        public void LoadLevel(int levelNumber)
        {
            currentLevel = levelNumber;
            LoadLevel("Levels/level_" + levelNumber + ".txt");
        }
        // -------------------------------------
        public void LoadLevel(string fileName)
        {
            // Clear the existing level
            ClearLevel();

            // Create a filestream for this level
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Load the level and ensure all of the lines are the same length.
            int width;
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line = reader.ReadLine();
                width = line.Length;
                while (line != null)
                {
                    lines.Add(line);
                    if (line.Length != width)
                        throw new Exception(String.Format("The length of line {0} is different from all preceeding lines.", lines.Count));
                    line = reader.ReadLine();
                }
            }

            // Allocate the tile grid.
            levelWidth = width;
            levelHeight = lines.Count;
            walls = new Wall[levelWidth, levelHeight];

            // Loop over every tile position,
            for (int y = 0; y < levelHeight; ++y)
            {
                for (int x = 0; x < levelWidth; ++x)
                {
                    // to load each tile.
                    char tileType = lines[y][x];
                    LoadTile(tileType, x, y);
                }
            }

            // Verify that the level has a beginning and an end.
            if (player == null)
                throw new NotSupportedException("A level must have a starting point.");
        }
        // -------------------------------------
        private void ClearLevel()
        {
            walls = null;
            coins.Clear();
            spikes.Clear();
            enemies.Clear();
        }
        // -------------------------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Blank space
                case '.':
                    break; // Do nothing!

                // Player
                case 'P':
                    ResetPlayer(tileX, tileY);
                    break;

                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                // Coin
                case 'C':
                    CreateCoin(tileX, tileY);
                    break;

                // Spike
                case 'S':
                    CreateSpike(tileX, tileY);
                    break;

                // Enemies
                case 'H':
                    CreateEnemy(tileX, tileY, Enemy.MovementType.HORIZONTAL);
                    break;
                case 'V':
                    CreateEnemy(tileX, tileY, Enemy.MovementType.VERTICAL);
                    break;
            }

        }
        // -------------------------------------
        public void ReloadLevel()
        {
            shouldReload = true;
        }
        // -------------------------------------
        public void GameOver()
        {
            game.ChangeScreen(Screen.GAMEOVER);
        }
        // -------------------------------------
    }
}
