﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class GameOverScreen
    {
        // ---------------------------
        // Data
        // ---------------------------
        Text message;
        Button restartButton;
        Button quitButton;
        Game1 game;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public void LoadContent(ContentManager content, GraphicsDevice device, Game1 newGame)
        {
            game = newGame;
            SpriteFont messageFont = content.Load<SpriteFont>("fonts/largeFont");
            
            // Create Message
            message = new Text(messageFont);
            message.SetAlignment(Text.Alignment.CENTRE);
            message.SetPosition(new Vector2(device.Viewport.Bounds.Width/2, device.Viewport.Bounds.Height / 2 - 50));
            message.SetString("GAME OVER");

            // Create buttons
            SpriteFont buttonFont = content.Load<SpriteFont>("fonts/mainFont");
            Texture2D buttonTexture = content.Load<Texture2D>("graphics/Button");
            Texture2D pressedTexture = content.Load<Texture2D>("graphics/Button_Pressed");
            SoundEffect buttonClickSFX = content.Load<SoundEffect>("audio/click");

            restartButton = new Button(buttonTexture, pressedTexture, buttonFont, buttonClickSFX);
            restartButton.SetPosition(new Vector2(device.Viewport.Bounds.Width / 2 - 100 - restartButton.GetBounds().Width / 2, device.Viewport.Bounds.Height / 2 + 50));
            restartButton.SetString("RESTART");

            quitButton = new Button(buttonTexture, pressedTexture, buttonFont, buttonClickSFX);
            quitButton.SetPosition(new Vector2(device.Viewport.Bounds.Width / 2 + 100 - quitButton.GetBounds().Width / 2, device.Viewport.Bounds.Height / 2 + 50));
            quitButton.SetString("QUIT");
        }
        // -------------------------------------
        public void Update(GameTime gameTime)
        {
            if (restartButton.IsClicked())
            {
                // Restart game
                game.ChangeScreen(Screen.LEVEL); // TODO: Change to title!
            }

            if (quitButton.IsClicked())
            {
                // Quit game
                game.Exit();
            }
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            message.Draw(spriteBatch);
            restartButton.Draw(spriteBatch);
            quitButton.Draw(spriteBatch);
        }
        // -------------------------------------
    }
}
