﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Text
    {
        // ---------------------------
        // Enums
        // ---------------------------
        public enum Alignment
        {
            TOP_LEFT,
            TOP,
            TOP_RIGHT,
            LEFT,
            CENTRE,
            RIGHT,
            BOTTOM_LEFT,
            BOTTOM,
            BOTTOM_RIGHT
        }


        // ---------------------------
        // Data
        // ---------------------------
        SpriteFont font;
        string textString;
        Vector2 position;
        Color color = Color.White;
        Alignment alignment = Alignment.TOP_LEFT;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Text(SpriteFont newFont)
        {
            font = newFont;
        }
        // ---------------------------
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            Vector2 adjustedPosition = position;
            Vector2 textSize = font.MeasureString(textString);

            switch(alignment)
            {
                case Alignment.TOP_LEFT:
                    // Do nothing, default behaviour
                    break;
                case Alignment.TOP:
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.TOP_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    break;
                case Alignment.LEFT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.CENTRE:
                    adjustedPosition.X -= textSize.X / 2;
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.RIGHT:
                    adjustedPosition.X -= textSize.X;
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.BOTTOM_LEFT:
                    adjustedPosition.Y -= textSize.Y;
                    break;
                case Alignment.BOTTOM:
                    adjustedPosition.X -= textSize.X / 2;
                    adjustedPosition.Y -= textSize.Y;
                    break;
                case Alignment.BOTTOM_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    adjustedPosition.Y -= textSize.Y;
                    break;
            }

            spriteBatch.DrawString(font, textString, adjustedPosition, color);
        }
        // ---------------------------
        public void SetString(string newString)
        {
            textString = newString;
        }
        // ---------------------------
        public void SetColor(Color newColor)
        {
            color = newColor;
        }
        // ---------------------------
        public void SetAlignment(Alignment newAlignment)
        {
            alignment = newAlignment;
        }
        // ---------------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ---------------------------

    }
}
