﻿using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class AnimatingSprite : Sprite
    {

        // ---------------------------
        // Types
        // ---------------------------
        struct Animation
        {
            public int startFrame;
            public int endFrame;
            public bool looping;
        }

        // ---------------------------
        // Data
        // ---------------------------

        // Settings
        private int frameWidth = 32;
        private int frameHeight = 32;
        private float framesPerSecond = 30;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();

        // Runtime
        private bool playing = true;
        private string currentAnimation = "";
        private int currentFrame = 0;
        private float timeInFrame = 0;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public AnimatingSprite(Texture2D newSprite, int newFrameWidth, int newFrameHeight, float newFPS) 
            : base(newSprite)
        {
            frameWidth = newFrameWidth;
            frameHeight = newFrameHeight;
            framesPerSecond = newFPS;
        }
        // ---------------------------
        public void AddAnimation(string name, int startFrame, int endFrame, bool isLooping = true)
        {
            Animation newAnimation = new Animation();
            newAnimation.startFrame = startFrame;
            newAnimation.endFrame = endFrame;
            newAnimation.looping = isLooping;
            animations.Add(name, newAnimation);
        }
        // ---------------------------
        public void PlayAnimation(string name, bool reset = false)
        {
            // Don't bother messing with things if the animation is the current one
            // Unless we've said we want to reset the animation
            if (name == currentAnimation && playing == true && reset == false)
                return;


            // Make sure this animation is in the list
            // If not, stop execution and inform the tester
            bool animationSetup = animations.ContainsKey(name);
            Debug.Assert(animationSetup, "Animation " + name + " not found in animation");

            // That will only happen in debug builds, so is good for testing and won't affect release
            // But this means we need to check for release as well and only do stuff if there is a clip
            if (animationSetup)
            {
                currentAnimation = name;
                currentFrame = animations[name].startFrame;
                timeInFrame = 0;
            }

            playing = true;
        }
        // ---------------------------
        public void PauseAnimation()
        {
            playing = false;
        }
        // ---------------------------
        public void StopAnimation()
        {
            playing = false;

            // Reset to first frame of the animation
            Animation thisAnimation = animations[currentAnimation];
            currentFrame = thisAnimation.startFrame;
        }
        // ---------------------------
        public virtual void Update(GameTime gameTime)
        {
            // Don't update if we aren't actually playing
            if (!playing)
                return;

            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Add to how long we've spent in this frame
            timeInFrame += frameTime;

            // Determine if it's time for a new frame
            float timePerFrame = 1.0f / framesPerSecond; // --> seconds per frame (inverse of frames per second)
            if (timeInFrame >= timePerFrame)
            {
                // It's time for a new frame
                Animation thisAnimation = animations[currentAnimation];
                ++currentFrame;
                timeInFrame = 0;
                // If this would cause us to pass our last frame...
                if (currentFrame > thisAnimation.endFrame)
                {
                    // If we're looping, return to the start of the animation
                    if (thisAnimation.looping == true)
                    {
                        currentFrame = thisAnimation.startFrame;
                    }
                    // If we are not looping, just stay on the last frame
                    else
                    {
                        currentFrame = thisAnimation.endFrame;
                    }
                }
            }
        }
        // ---------------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible == false)
                return; // Don't draw if not visible!

            int numFramesX = texture.Width / frameWidth;
            int xFrameIndex = currentFrame % numFramesX;
            int yFrameIndex = currentFrame / numFramesX;

            Rectangle source = new Rectangle(xFrameIndex * frameWidth, yFrameIndex * frameHeight, frameWidth, frameHeight);

            spriteBatch.Draw(texture, position, source, Color.White);

            // do NOT call base.Draw, as we do not want Sprite's draw behaviour.
        }
        // ---------------------------
        public override Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);
        }
        // ---------------------------
    }
}
