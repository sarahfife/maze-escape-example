﻿using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Coin : Sprite
    {
        // ---------------------------
        // Data
        // ---------------------------
        int value = 100;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Coin(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ---------------------------
        public int GetValue()
        {
            return value;
        }
        // ---------------------------
    }
}
