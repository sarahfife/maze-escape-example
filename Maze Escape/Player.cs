﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Player : Actor
    {
        // ---------------------------
        // Data
        // ---------------------------
        int score = 0;
        int scoreAtLevelStart = 0;
        int lives = STARTING_LIVES;
        Level level = null;

        // Constants
        private const float MOVE_ACCEL = 100000.0f;
        private const float MOVE_DRAG_FACTOR = 0.48f;
        private const float MAX_MOVE_SPEED = 500.0f;
        private const int STARTING_LIVES = 3;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFPS, Level newLevel)
            : base(newTexture, newFrameWidth, newFrameHeight, newFPS)
        {
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            PlayAnimation("walkDown");
            StopAnimation();

            level = newLevel;
        }
        // ---------------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Get current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;
            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                PlayAnimation("walkLeft");
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                PlayAnimation("walkRight");
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                PlayAnimation("walkUp");
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                PlayAnimation("walkDown");
            }

            // Add the movement change to the velocity
            velocity += movementInput * MOVE_ACCEL * frameTime;

            // Apply drag from ground
            velocity *= MOVE_DRAG_FACTOR;

            // If the speed is too high, clamp it
            if (velocity.Length() > MAX_MOVE_SPEED)
            {
                velocity.Normalize();
                velocity = velocity * MAX_MOVE_SPEED;
            }

            // if the player isn't moving, stop the animation
            if (velocity.Length() < 10f)
            {
                StopAnimation();
            }

            base.Update(gameTime);
        }
        // ---------------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude.
            Rectangle tileBounds = hitWall.GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(GetBounds());

            // If the depth is not zero, it means we are colliding with this tile
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the 
                // one we're closer to the edge on and therefore easier to "squeeze out"
                // First we check the vertical axis
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision along the Y axis.
                    position = new Vector2(position.X, position.Y + depth.Y);
                }
                else
                {
                    // Resolve the collision along the X axis.
                    position = new Vector2(position.X + depth.X, position.Y);
                }
            }
        }
        // ---------------------------
        public void HandleCollision(Coin hitCoin)
        {
            // Add the coin's value to our score
            score += hitCoin.GetValue();

            // Hide the coin so we can't hit it again and it looks like it has been collected
            hitCoin.SetVisible(false);
        }
        // ---------------------------
        public int GetScore()
        {
            return score;
        }
        // ---------------------------
        public void KillPlayer()
        {
            score = scoreAtLevelStart;
            --lives;
            // restart level
            level.ReloadLevel();

            if (lives == 0)
            {
                score = 0;
                lives = STARTING_LIVES;
                level.GameOver();
            }
        }
        // ---------------------------
        public int GetLives()
        {
            return lives;
        }
        // ---------------------------
        public void SetLives(int newLives)
        {
            lives = newLives;
        }
        // ---------------------------
    }
}
