﻿using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Spikes : Sprite
    {
        // ---------------------------
        // Behaviour
        // ---------------------------
        public Spikes(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ---------------------------
        public void HandleCollision(Player hitPlayer)
        {
            hitPlayer.KillPlayer();
        }
        // ---------------------------
    }
}
