﻿using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class Enemy : Actor
    {
        // ---------------------------
        // Enums
        // ---------------------------
        public enum MovementType
        {
            HORIZONTAL,
            VERTICAL
        }
        // ---------------------------


        // ---------------------------
        // Data
        // ---------------------------
        // Constants
        private const int SPEED = 100;
        // ---------------------------


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Enemy(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFPS, MovementType movementType)
            : base(newTexture, newFrameWidth, newFrameHeight, newFPS)
        {
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);
            
            switch(movementType)
            {
                case MovementType.HORIZONTAL:
                    velocity.X = SPEED;
                    PlayAnimation("walkRight");
                    break;
                case MovementType.VERTICAL:
                    velocity.Y = SPEED;
                    PlayAnimation("walkDown");
                    break;
            }

        }
        // ---------------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude.
            Rectangle tileBounds = hitWall.GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(GetBounds());

            // If the depth is not zero, it means we are colliding with this tile
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the 
                // one we're closer to the edge on and therefore easier to "squeeze out"
                // First we check the vertical axis
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision along the Y axis.
                    position = new Vector2(position.X, position.Y + depth.Y);

                    // Start moving in the opposite direction
                    velocity.Y = -1 * velocity.Y;

                    // Choose the correct animation
                    if (velocity.Y > 0)
                        PlayAnimation("walkDown");
                    else
                        PlayAnimation("walkUp");

                }
                else
                {
                    // Resolve the collision along the X axis.
                    position = new Vector2(position.X + depth.X, position.Y);

                    // Start moving in the opposite direction
                    velocity.X = -1 * velocity.X;

                    // Choose the correct animation
                    if (velocity.X > 0)
                        PlayAnimation("walkRight");
                    else
                        PlayAnimation("walkLeft");
                }
            }
        }
        // ---------------------------
        public void HandleCollision(Player hitPlayer)
        {
            hitPlayer.KillPlayer();
        }
        // ---------------------------
    }
}
