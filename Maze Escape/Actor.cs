﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Actor : AnimatingSprite
    {
        // ---------------------------
        // Data
        // ---------------------------
        protected Vector2 velocity;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Actor(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFPS)
            : base(newTexture, newFrameWidth, newFrameHeight, newFPS)
        {
        }
        // ---------------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            
            // Update position based on velocity and frame time
            position += velocity * frameTime;

            base.Update(gameTime);
        }
    }
}
