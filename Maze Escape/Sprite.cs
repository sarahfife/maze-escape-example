﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Sprite
    {
        // ---------------------------
        // Data
        // ---------------------------
        protected Texture2D texture;
        protected Vector2 position;
        protected bool visible = true;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Sprite(Texture2D newTexture)
        {
            texture = newTexture;
        }
        // ---------------------------
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
                spriteBatch.Draw(texture, position, Color.White);
        }
        // ---------------------------
        public bool GetVisible()
        {
            return visible;
        }
        // ---------------------------
        public void SetVisible(bool newVisible)
        {
            visible = newVisible;
        }
        // ---------------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ---------------------------
        public virtual Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }
        // ---------------------------
        public Vector2 GetCollisionDepth(Rectangle otherRect)
        {
            // The amount of overlap between two intersecting rectangles. These
            // depth values can be negative depending on which wides the rectangles
            // intersect. This allows callers to determine the correct direction
            // to push objects in order to resolve collisions.
            // If the rectangles are not intersecting, Vector2.Zero is returned.

            // Check will be between tile Bounds and colliding (other) rectangle
            Rectangle tileBounds = GetBounds();

            // Calculate half sizes.
            float halfWidthOther = otherRect.Width / 2.0f;
            float halfHeightOther = otherRect.Height / 2.0f;
            float halfWidthTile = tileBounds.Width / 2.0f;
            float halfHieghtTile = tileBounds.Height / 2.0f;

            // Calculate centers.
            Vector2 centerOther = new Vector2(otherRect.Left + halfWidthOther, otherRect.Top + halfHeightOther);
            Vector2 centerTile = new Vector2(tileBounds.Left + halfWidthTile, tileBounds.Top + halfHieghtTile);

            // Calculate current and minimum-non-intersecting distances between centers.

            // Distance between the centers of A and B, on the X and Y coordinate
            float distanceX = centerOther.X - centerTile.X;
            float distanceY = centerOther.Y - centerTile.Y;

            // Minimum distance these need to be to NOT intersect
            // AKA if they are AT LEAST this far away in either direction, they are not touching.
            float minDistanceX = halfWidthOther + halfWidthTile;
            float minDistanceY = halfHeightOther + halfHieghtTile;

            // If we are not intersecting at all, return (0, 0).
            // If either the X or Y distance is greater than their minimum, we are NOT intersecting
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
                return Vector2.Zero;

            // Calculate and return intersection depths.
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersection in that direction?
            float depthX, depthY;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        // ---------------------------
        public void DrawBounds(GraphicsDevice graphics, SpriteBatch spriteBatch)
        {
            Rectangle bounds = GetBounds();

            Texture2D rect = new Texture2D(graphics, bounds.Width, bounds.Height);

            Color[] data = new Color[bounds.Width * bounds.Height];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.White;
            rect.SetData(data);

            Vector2 coor = new Vector2(bounds.X, bounds.Y);
            spriteBatch.Draw(rect, coor, Color.White);
        }
        // ---------------------------
        // ---------------------------
    }
}
