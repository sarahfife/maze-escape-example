﻿using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Wall : Sprite
    {
        // ---------------------------
        // Data
        // ---------------------------
        

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Wall(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ---------------------------

    }
}
